# MeFitMiniCase
An  ASP.NET Core Web API which provides end points for an exercise plan profile and management og the data.

## Functionalities
- Provides Create, Read, Update and Delete (CRUD) endpoints for the profile class
- Persists data using Entity Framework in a exercise management app.

## Database diagram
![Database diagram](/MeFitMiniCase/images/db_diagram.jpg "Logo Title Text 1")

## Controllers 
- **ProfilesController**
The main controller. Controles communication with the database which relates to the profile model. GET requests return address and user information.

- **AddressController**
Controles communication with the database which relates to the address model
Automatically built skeleton, GET requests returns only pure address information. 

- **ProgramsController**
Controles communication with the database which relates to the program model
Automatically built skeleton, GET requests returns only pure program information.

- **WorkoutsController**
Controles communication with the database which relates to the workout model
Automatically built skeleton, GET requests returns only pure workout information.


## Models
### Profile
Model for managing information on a profile. Holds the following data:
  * Medical conditions
  * Disabilities { get; set; }
  * Weight (in kg)
  * Height (in cm)

In addition it holds references to the following other models:
  * Address (one-to-one)
  * User (one-to-one)
  * Goal (one-to-many)
  * Program (one-to-many)
  * Workout (one-to-many)

### Address
Model for managing information about an address. Holds the following data:
  * AddressLine 1
  * AddressLine 2
  * AddressLine 3
  * Postal code
  * City
  * Country
In addition it holds references to the following other models:
  * Profile (one-to-one)

### User
Model for managing information about a user. Holds the following data:
  * Password
  * First name
  * Last name
  * If the user is an admin
  * If the user is an contributor

In addition it holds references to the following other models:
  * Profile (one-to-one)

### Exercise
Model for managing information about an exercise. Holds the following data:
  * Name
  * Description
  * Target muscle group
  * Image location
  * Video link

In addition it holds references to the following other models:
  * Set (one-to-many)

### Goal
Model for managing information about a goal. Holds the following data:
  * End date
  * Information on wether it is achieved or not

In addition it holds references to the following other models:
  * Workout (many-to-many)
  * Profile (one-to-many)

### GoalWorkout
Model for managing the many to many relationship between the goal and workout models.

### Workout
Model for managing information about a workout. Holds the following data:
  * Name
  * Type
  * Information on wether it is complete or not

In addition it holds references to the following other models:
  * Set (many-to-many)
  * Goal (many-to-many)
  * Program (many-to-many)
  * Profile (one-to-many)

### Program
Model for managing information about a workout. Holds the following data:
  * Name
  * Category

In addition it holds references to the following other models:
  * Workout (many-to-many)

### ProgramWorkout
Model for managing the many to many relationship between the program and workout models.


### Set
Model for managing information about a workout. Holds the following data:
  * Exercise repetitions
  * Number of sets

In addition it holds references to the following other models:
  * Workout (many-to-many)
  * Exercise (one-to-many)
  * Profile (one-to-many)


### SetWorkout
Model for managing the many to many relationship between the set and workout models.


## Author
Ole Baadshaug