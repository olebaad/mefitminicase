﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitMiniCase.Migrations
{
    public partial class MyFifthMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "AddressLine1", "AddressLine2", "AddressLine3", "City", "Country", "PostalCode" },
                values: new object[,]
                {
                    { 1, "Bargen 1", null, null, "Bergen", "Pakistan", 0 },
                    { 2, "Bargen 2", null, null, "Bergen", "Finland", 0 },
                    { 3, "Bargen 3", null, null, "Bergen", "Norge", 0 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "FirstName", "IsAdmin", "IsContributor", "LastName", "Password" },
                values: new object[,]
                {
                    { 1, "Ole", true, true, "Boss", "1234" },
                    { 2, "Balle", false, true, "Klorin", "fffffu" },
                    { 3, "Fredrik", false, false, "Jacobsen", "Hoppbakke" }
                });

            migrationBuilder.InsertData(
                table: "Profiles",
                columns: new[] { "Id", "AddressId", "Disabilities", "GoalId", "HeightInCm", "MedicalConditions", "ProgramId", "SetId", "UserId", "WeightInKg", "WorkoutId" },
                values: new object[] { 1, 1, "none", null, 178, "none", null, null, 1, 83, null });

            migrationBuilder.InsertData(
                table: "Profiles",
                columns: new[] { "Id", "AddressId", "Disabilities", "GoalId", "HeightInCm", "MedicalConditions", "ProgramId", "SetId", "UserId", "WeightInKg", "WorkoutId" },
                values: new object[] { 2, 2, "Stupid", null, 180, "Astma", null, null, 2, 98, null });

            migrationBuilder.InsertData(
                table: "Profiles",
                columns: new[] { "Id", "AddressId", "Disabilities", "GoalId", "HeightInCm", "MedicalConditions", "ProgramId", "SetId", "UserId", "WeightInKg", "WorkoutId" },
                values: new object[] { 3, 3, "none", null, 190, "Cancer", null, null, 3, 70, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Addresses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
