﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MeFitMiniCase.Models;


namespace MeFitMiniCase.Data
{
    public class MeFitDbContext : DbContext
    {
        public MeFitDbContext(DbContextOptions<MeFitDbContext> options)
            : base(options)
        {
        }

        public DbSet<MeFitMiniCase.Models.Profile> Profiles { get; set; }
        public DbSet<MeFitMiniCase.Models.Address> Addresses { get; set; }
        public DbSet<MeFitMiniCase.Models.User> Users { get; set; }
        public DbSet<MeFitMiniCase.Models.Exercise> Exercises { get; set; }
        public DbSet<MeFitMiniCase.Models.Goal> Goals { get; set; }
        public DbSet<MeFitMiniCase.Models.GoalWorkout> GoalWorkouts { get; set; }
        public DbSet<MeFitMiniCase.Models.Program> Programs { get; set; }
        public DbSet<MeFitMiniCase.Models.ProgramWorkout> ProgramWorkouts { get; set; }
        public DbSet<MeFitMiniCase.Models.Set> Sets { get; set; }
        public DbSet<MeFitMiniCase.Models.Workout> Workouts { get; set; }
        public DbSet<MeFitMiniCase.Models.SetWorkout> SetWorkouts { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GoalWorkout>().HasKey(ps => new { ps.WorkoutId, ps.GoalId });
            modelBuilder.Entity<SetWorkout>().HasKey(ps => new { ps.WorkoutId, ps.SetId });
            modelBuilder.Entity<ProgramWorkout>().HasKey(ps => new { ps.WorkoutId, ps.ProgramId });

            modelBuilder.Entity<Profile>().HasData(new Profile() { Id = 1, UserId = 1, AddressId = 1, MedicalConditions = "none", Disabilities = "none", WeightInKg = 83, HeightInCm = 178 });
            modelBuilder.Entity<Profile>().HasData(new Profile() { Id = 2, UserId = 2, AddressId = 2, MedicalConditions = "Astma", Disabilities = "Stupid", WeightInKg = 98, HeightInCm = 180 });
            modelBuilder.Entity<Profile>().HasData(new Profile() { Id = 3, UserId = 3, AddressId = 3, MedicalConditions = "Cancer", Disabilities = "none", WeightInKg = 70, HeightInCm = 190 });

            modelBuilder.Entity<User>().HasData(new User() { Id = 1, Password = "1234", FirstName = "Ole", LastName = "Boss", IsContributor = true, IsAdmin = true });
            modelBuilder.Entity<User>().HasData(new User() { Id = 2, Password = "fffffu", FirstName = "Balle", LastName = "Klorin", IsContributor = true, IsAdmin = false });
            modelBuilder.Entity<User>().HasData(new User() { Id = 3, Password = "Hoppbakke", FirstName = "Fredrik", LastName = "Jacobsen", IsContributor = false, IsAdmin = false });

            modelBuilder.Entity<Address>().HasData(new Address() { Id = 1, AddressLine1 = "Bargen 1", City = "Bergen", Country = "Pakistan" });
            modelBuilder.Entity<Address>().HasData(new Address() { Id = 2, AddressLine1 = "Bargen 2", City = "Bergen", Country = "Finland" });
            modelBuilder.Entity<Address>().HasData(new Address() { Id = 3, AddressLine1 = "Bargen 3", City = "Bergen", Country = "Norge" });
        }

    }
}
