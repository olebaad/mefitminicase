﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCase.Models
{
    public class Goal
    {
        public int Id { get; set; }
        
        public DateTime EndDate { get; set; }

        public bool Achieved { get; set; }

        public ICollection<GoalWorkout> GoalWorkout { get; set; }
       
        public ICollection<Profile> Profile { get; set; }

    }
}
