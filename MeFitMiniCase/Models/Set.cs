﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCase.Models
{
    public class Set
    {
        public int Id { get; set; }

        public int ExerciseRepetitions { get; set; }

        public int NumberOfSets { get; set; }

        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }

        public ICollection<SetWorkout> SetWorkouts { get; set; }

        public ICollection<Profile> Profile { get; set; }
    }
}
