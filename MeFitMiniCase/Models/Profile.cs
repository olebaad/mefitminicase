﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace MeFitMiniCase.Models
{
    public class Profile
    {
        public int Id { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public int? GoalId { get; set; }
        public Goal Goal { get; set; }

        public int? AddressId { get; set; }
        public Address Address { get; set; }

        public int? ProgramId { get; set; }
        public Program Program { get; set; }

        public int? WorkoutId { get; set; }
        public Workout Workout { get; set; }

        public string MedicalConditions { get; set; }

        public string Disabilities { get; set; }

        public int WeightInKg { get; set; }

        public int HeightInCm { get; set; }
    }
}
