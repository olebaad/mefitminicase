﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCase.Models
{
    public class SetWorkout
    {
        public Workout Workout { get; set; }
        public int WorkoutId { get; set; }

        public Set Set { get; set; }
        public int SetId { get; set; }
    }
}
