﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCase.Models
{
    public class ProgramWorkout
    {
        public Program Program { get; set; }
        public int ProgramId { get; set; }

        public Workout Workout { get; set; }
        public int WorkoutId { get; set; }
    }

}
