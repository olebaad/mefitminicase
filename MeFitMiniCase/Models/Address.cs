﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MeFitMiniCase.Models
{
    public class Address
    {
        public int Id { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public int PostalCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public Profile Profile { get; set; }

    }
}
