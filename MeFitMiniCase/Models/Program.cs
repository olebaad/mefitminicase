﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MeFitMiniCase.Models
{
    public class Program
    {
        public int Id { get; set; }

        public string Name { get; set; }

        //You could implement a dropdown menu here
        public string Category { get; set; }

        public ICollection<ProgramWorkout> ProgramWorkout { get; set; }

        public ICollection<Profile> Profile { get; set; }
    }
}
